#include <stdio.h>

/*
 * Dan Mechanic
 * Showing Size of Different Variable Type
 */

int main() {
  printf("int %ld\n", sizeof(int));
  printf("char %ld\n", sizeof(char));
  printf("short %ld\n", sizeof(short));
  printf("long %ld\n", sizeof(long));
  printf("float %ld\n", sizeof(float));
  printf("double %ld\n", sizeof(double));
}
