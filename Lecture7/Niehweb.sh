#!/bin/bash -x


TMPDIR=tmpnieh
mkdir $TMPDIR

cd tmpnieh
BASEURL=www.cs.columbia.edu/~nieh/

# Download all the papers
for i in $(curl -s $BASEURL | grep -o pubs.*pdf);
do
    curl -o $(basename ${i}) $BASEURL/${i} 2>/dev/null
done

# turn them all to text files
for i in $(ls *pdf);
do
    pdftotext $i;
    rm $i;
done

# download stopwords
curl -o stopwords http://www.columbia.edu/~dm2474/stopwords

cat *.txt | tr ' ' '\n' | grep -v -w -f stopwords | sort | uniq -c | sort -nr | less






