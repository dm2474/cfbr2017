#!/usr/bin/gnuplot -persist

username=system('whoami;date');
set title username;
set style data histogram;
set style fill solid;
plot './taxizones.data' using 1:xtic(2) title columnheader(2)
